package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	log "github.com/Sirupsen/logrus"
	simplejson "github.com/bitly/go-simplejson"
)

// getMetric tests
func TestGetMetricWrongResponseStatus(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))
	defer ts.Close()

	testURL := ts.URL

	log.Info(testURL)
	c := EOSKPIPublisher{}
	c.GrafanaEndpoint = testURL
	_, err := c.getMetric("test")
	if err == nil {
		t.Errorf("getMetric didn't return an error")
	}
}
func TestGetMetricBadJSON(t *testing.T) {
	respJSON := `
	{
		"target": "prout",
		"datapoints": "bad format"
	}
	`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		if r.Method != "GET" {
			t.Errorf("Expected 'GET' request, got: '%s'", r.Method)
		}
		r.ParseForm()

		target := r.Form.Get("target")
		if target != "prout" {
			t.Errorf("Expected request to have 'target=prout', got: '%s'", target)
		}

		w.Write([]byte(respJSON))
	}))
	defer ts.Close()

	testURL := ts.URL
	c := EOSKPIPublisher{}
	c.GrafanaEndpoint = testURL

	_, err := c.getMetric("prout")
	if err == nil {
		t.Errorf("getMetric() didn't return an error")
	}
}
func TestGetMetricNoData(t *testing.T) {
	respJSON := `
	[{
		"target": "prout",
		"datapoints": [
		]
	}]
	`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		if r.Method != "GET" {
			t.Errorf("Expected 'GET' request, got: '%s'", r.Method)
		}
		r.ParseForm()

		target := r.Form.Get("target")
		if target != "prout" {
			t.Errorf("Expected request to have 'target=prout', got: '%s'", target)
		}

		w.Write([]byte(respJSON))
	}))
	defer ts.Close()

	testURL := ts.URL
	c := EOSKPIPublisher{}
	c.GrafanaEndpoint = testURL

	_, err := c.getMetric("prout")
	if err.Error() != "No data" {
		t.Errorf("getMetric() didn't return expected error: %s", err)
	}
}
func TestGetMetricNullData(t *testing.T) {
	respJSON := `
	[{
		"target": "prout",
		"datapoints": [
			[null, 1311836012]
		]
	}]
	`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		if r.Method != "GET" {
			t.Errorf("Expected 'GET' request, got: '%s'", r.Method)
		}
		r.ParseForm()

		target := r.Form.Get("target")
		if target != "prout" {
			t.Errorf("Expected request to have 'target=prout', got: '%s'", target)
		}

		w.Write([]byte(respJSON))
	}))
	defer ts.Close()

	testURL := ts.URL
	c := EOSKPIPublisher{}
	c.GrafanaEndpoint = testURL

	d, err := c.getMetric("prout")
	if err != nil {
		t.Errorf("getMetric() returned an error: %s", err)
	}
	fmt.Print(d)
}

func TestGetMetricOK(t *testing.T) {
	respJSON := `
	[{
		"target": "prout",
		"datapoints": [
			[1.0, 1311836008],
			[2.0, 1311836009],
			[3.0, 1311836010],
			[5.0, 1311836011],
			[6.0, 1311836012]
		]
	}]
	`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		if r.Method != "GET" {
			t.Errorf("Expected 'GET' request, got: '%s'", r.Method)
		}
		r.ParseForm()

		target := r.Form.Get("target")
		if target != "prout" {
			t.Errorf("Expected request to have 'target=prout', got: '%s'", target)
		}

		w.Write([]byte(respJSON))
	}))
	defer ts.Close()

	testURL := ts.URL
	c := EOSKPIPublisher{}
	c.GrafanaEndpoint = testURL

	_, err := c.getMetric("prout")
	if err != nil {
		t.Errorf("getMetric() returned an error: %s", err)
	}
}

func TestGenerateJSONinvalid(t *testing.T) {
	invalidData := make(chan int) // Can't marshal a channel !
	c := EOSKPIPublisher{}
	_, err := c.generateJSON(invalidData)
	if err == nil {
		t.Errorf("Returned nothing, expected error !")
	}
}

func TestGenerateJSONValid(t *testing.T) {
	validData := map[string]interface{}{
		"key": 42,
	}
	c := EOSKPIPublisher{}
	_, err := c.generateJSON(validData)
	if err != nil {
		t.Errorf("Returned error '%s', expected nothing", err)
	}
}
func TestPushKPIsWrongResponseStatus(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusServiceUnavailable)
	}))
	defer ts.Close()

	testURL := ts.URL

	log.Info(testURL)
	c := EOSKPIPublisher{}
	c.KPIendpoint = testURL
	err := c.pushKPIs([]byte("test"))
	if err == nil {
		t.Errorf("pushKPIs() didn't return an error")
	}
}
func TestPushKPIsBadJSON(t *testing.T) {
	badKPIs := `
	[
		{
			"invalid": 42
		}
	]
	`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			t.Errorf("Expected 'POST' request, got: '%s'", r.Method)
		}
		r.ParseForm()
		reqJSON, err := simplejson.NewFromReader(r.Body)
		if err != nil {
			t.Errorf("Error while reading request JSON: %s", err)
		}
		testProducer := reqJSON.GetPath("producer").MustString()
		if testProducer != "EOS" {
			w.WriteHeader(http.StatusBadRequest)
		}
	}))
	defer ts.Close()

	testURL := ts.URL
	c := EOSKPIPublisher{}
	c.KPIendpoint = testURL

	err := c.pushKPIs([]byte(badKPIs))
	if err == nil {
		t.Errorf("pushKPIs() didn't return an error")
	}
}
func TestPushKPIsOK(t *testing.T) {
	goodKPIs := `
	[
		{
			"producer":    "eos",
			"timestamp":   123456789,
			"type_prefix": "raw",
			"type":        "metrics",
			"experiment":  "pps",
			"idb_tags":    "experiment",
			"idb_fields":  ["kpi1", "kpi2", "kpi3"],
			"kpi1": 42,
			"kpi2": 42,
			"kpi3": 42
		}
	]
	`
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "POST" {
			t.Errorf("Expected 'POST' request, got: '%s'", r.Method)
		}
		r.ParseForm()
		reqJSON, err := simplejson.NewFromReader(r.Body)
		if err != nil {
			t.Errorf("Error while reading request JSON: %s", err)
		}
		testProducer := reqJSON.GetIndex(0).GetPath("producer").MustString()
		if testProducer != "eos" {
			w.WriteHeader(http.StatusBadRequest)
		}
		testDataKPI1 := reqJSON.GetIndex(0).GetPath("kpi1").MustInt()
		if testDataKPI1 != 42 {
			w.WriteHeader(http.StatusBadRequest)
		}
	}))
	defer ts.Close()

	testURL := ts.URL
	c := EOSKPIPublisher{}
	c.KPIendpoint = testURL

	err := c.pushKPIs([]byte(goodKPIs))
	if err != nil {
		t.Errorf("pushKPIs() returned an error: %s", err)
	}
}

FROM scratch

ADD ./misc/config.yaml /
ADD ./eos-kpis /

CMD ["/eos-kpis"]

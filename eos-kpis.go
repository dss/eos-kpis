package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/http/pprof"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/heptiolabs/healthcheck"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"go.opencensus.io/exporter/jaeger"
	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/trace"
	"go.opencensus.io/zpages"
)

var debug bool
var oneshot bool
var tracing bool
var healthport int
var ctx context.Context

// EOSKPIPublisher contains the state and
// configuration of the current app
type EOSKPIPublisher struct {
	Timeout         int
	GrafanaEndpoint string
	APIKey          string
	KPIendpoint     string
	Instances       []string
	Metrics         map[string]string
}

// JSONMetric holds the query and associated datapoints
// in the format (value, timestamp)
type JSONMetric struct {
	Target     string      `json:"target"`
	Datapoints [][]float64 `json:"datapoints"`
}

// MetricJob is used to keep job-related info for
// concurrent querying of the Grafana API
type MetricJob struct {
	Instance string
	Key      string
	Query    string
}

// MetricResult is used to keep job-related info for
// concurrent querying of the Grafana API
type MetricResult struct {
	Instance string
	Key      string
	Value    float64
}

func init() {
	// Set defaults
	viper.SetDefault("timeout", 5)
	viper.SetDefault("interval", 300)
	viper.SetDefault("grafanaEndpoint", "http://localhost/grafana/proxy/render")
	viper.SetDefault("apikey", "")
	viper.SetDefault("KPIendpoint", "http://monit-metrics.cern.ch:10012/")
	viper.SetDefault("instances", []string{"pps"})
	viper.SetDefault("healthport", os.Getenv("PORT0"))
	viper.SetDefault("collectorEndpoint", "http://localhost:9411/")
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/eos-kpis/")
	viper.AddConfigPath("$HOME/.eos-kpis")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		log.Error("Unable to parse config file: ", err)
		os.Exit(1)
	}

	// Set command line flags
	pflag.BoolVar(&debug, "debug", false, "enable debug mode")
	pflag.BoolVar(&tracing, "tracing", false, "enable traces generation (needs to have collectorEndpoint set in config file)")
	pflag.BoolVar(&oneshot, "oneshot", false, "only run once")
	pflag.IntVar(&healthport, "healthport", viper.GetInt("healthport"), "health check")
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	// Env vars, prefixed with EOS_
	viper.SetEnvPrefix("EOS")
	viper.AutomaticEnv()
}

func main() {
	if debug {
		log.SetLevel(log.DebugLevel)
	}
	log.Debug("Environment variables:")
	for _, e := range os.Environ() {
		log.Debug(e)
	}
	log.Info("Initializing with following parameters:")
	log.Infof("Grafana endpoint: %s", viper.GetString("grafanaEndpoint"))
	log.Infof("KPI endpoint: %s", viper.GetString("KPIendpoint"))
	log.Infof("Instances: %s", viper.GetStringSlice("instances"))

	ctx := context.Background()

	if oneshot {
		log.Debugf("Running in one-shot mode...")
		schedule(ctx)
	} else {

		if tracing {
			// Tracing machinery
			log.Infof("Registering tracking exporter to: %s...", viper.GetString("collectorEndpoint"))

			je, err := jaeger.NewExporter(jaeger.Options{
				Endpoint:    viper.GetString("collectorEndpoint"),
				ServiceName: "eos-kpis",
			})
			if err != nil {
				log.Fatalf("Unable to create Jaeger exporter: %v", err)
			}
			trace.RegisterExporter(je)
			defer je.Flush()

			trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})

			// Create view for net/http
			if err := view.Register(
				ochttp.DefaultClientViews...,
			); err != nil {
				log.Fatalf("Failed to register HTTP client views: %v", err)
			}
		}

		// Healthz
		mux := http.NewServeMux()
		health := healthcheck.NewHandler()
		log.Infof("Starting health-check on %d...", viper.GetInt("healthport"))
		health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(100))

		zpages.Handle(mux, "/debug")
		mux.HandleFunc("/live", health.LiveEndpoint)
		mux.HandleFunc("/ready", health.ReadyEndpoint)
		mux.HandleFunc("/debug/pprof/", pprof.Index)
		mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
		mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		mux.HandleFunc("/debug/pprof/trace", pprof.Trace)

		healthBind := fmt.Sprintf("0.0.0.0:%d", viper.GetInt("healthport"))
		go http.ListenAndServe(healthBind, mux)

		log.Infof("Running forever at %ds intervals...", viper.GetInt("interval"))
		for {
			schedule(ctx)
			log.Debugf("Sleeping %ds...", viper.GetInt("interval"))
			time.Sleep(viper.GetDuration("interval") * time.Second)
		}
	}
	log.Info("Exiting...")
}

func schedule(ctx context.Context) {
	ctx, span := trace.StartSpan(ctx, "schedule")
	defer span.End()

	c := EOSKPIPublisher{}
	c.Timeout = viper.GetInt("timeout")
	c.KPIendpoint = viper.GetString("KPIEndpoint")
	c.GrafanaEndpoint = viper.GetString("grafanaEndpoint")
	c.APIKey = viper.GetString("apikey")
	c.Instances = viper.GetStringSlice("instances")
	c.Metrics = viper.GetStringMapString("metrics")

	log.Debugf("Fetching %d metrics for the following instances: %s", len(c.Metrics), c.Instances)

	var KPIs []interface{}

	// Worker pool
	queryChan := make(chan MetricJob, 10)
	resChan := make(chan MetricResult, 10)
	errChan := make(chan error, 10)

	var wg sync.WaitGroup
	for w := 0; w < 8; w++ {
		wg.Add(1)
		go c.getMetric(ctx, &wg, queryChan, resChan, errChan)
	}

	// Submit GetMetrics jobs
	go func() {
		for _, i := range c.Instances {
			for k, v := range c.Metrics {
				// tpl: replace all occurences of $instance with the instance name
				query := strings.Replace(v, "$instance", i, -1)
				log.Debugf("Fetching %s for %s", query, i)
				queryChan <- MetricJob{
					Instance: i,
					Key:      k,
					Query:    query,
				}
			}
		}
		close(queryChan)
	}()

	// Wait for completion
	go func() {
		wg.Wait()
		close(resChan)
	}()

	// Parse results
	for res := range resChan {
		instkpis := map[string]interface{}{
			"producer":    "eos",
			"service":     "eos",
			"type_prefix": "raw",
			"type":        "metrics",
			"experiment":  res.Instance,
			"timestamp":   time.Now().UnixNano() / int64(time.Millisecond),
			"idb_tags":    []string{"experiment", "service"},
		}
		log.Infof("%s/%s: %f ", res.Instance, res.Key, res.Value)
		// Populate meansurement value
		instkpis[res.Key] = res.Value
		// Populate idb_values with measurement name
		instkpis["idb_fields"] = []string{res.Key}
		KPIs = append(KPIs, instkpis)
	}

	// Build JSON
	j, err := c.generateJSON(ctx, KPIs)
	if err != nil {
		span.SetStatus(trace.Status{Code: trace.StatusCodeInternal, Message: err.Error()})
		log.Warn("Unable to generate JSON: %s", err)
	}

	if err := c.pushKPIs(ctx, j); err != nil {
		span.SetStatus(trace.Status{Code: trace.StatusCodeUnavailable, Message: err.Error()})
		log.Error("Unable to push KPIs: ", err)
	}
}

// pushKPIs does the actual POST of the data to the monitoring infrastructure
func (c EOSKPIPublisher) pushKPIs(ctx context.Context, data []byte) error {
	ctx, span := trace.StartSpan(ctx, "pushKPIs")
	defer span.End()

	req, err := http.NewRequest("POST", c.KPIendpoint, bytes.NewBuffer(data))
	if err != nil {
		log.Error("Unable to create HTTP request: ", err)
		return err
	}
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	req.WithContext(ctx)

	if debug {
		rd, err := httputil.DumpRequest(req, true)
		if err != nil {
			log.Error("Unable to dump HTTP Request: ", err)
		}
		log.Debug("HTTP Request:\n", string(rd))
	}

	client := &http.Client{
		Timeout:   time.Duration(c.Timeout) * time.Second,
		Transport: &ochttp.Transport{
			FormatSpanName: func(req *http.Request) string {
				return fmt.Sprintf("http-%s%s", req.URL.Host, req.URL.Path)
			},
		},
	}
	res, err := client.Do(req)
	if err != nil {
		log.Error("Unable to POST request: ", err)
		return err
	}
	defer res.Body.Close()

	if debug {
		rd, err := httputil.DumpResponse(res, true)
		if err != nil {
			log.Error("Unable to dump HTTP Response: ", err)
		}
		log.Debug("HTTP Response:\n", string(rd))
	}

	if res.StatusCode != http.StatusOK {
		log.Error("Data not accepted by remote end: ", res.Status)
		return errors.New("Remote HTTP error code: %s" + res.Status)
	}

	return nil
}

// generateJSON gets a map of k/v and puts them in
// the correct format expected by the Monitoring infrastructure
func (c EOSKPIPublisher) generateJSON(ctx context.Context, kpis interface{}) ([]byte, error) {
	ctx, span := trace.StartSpan(ctx, "generateJSON")
	defer span.End()

	jsonKpi, err := json.Marshal(kpis)
	if err != nil {
		log.Error("Unable to convert to JSON: ", err)
		return nil, err
	}

	log.Debugf("Generated KPIs: \n%s", jsonKpi)

	return jsonKpi, nil
}

// getMetric returns a value after querying the specified endpoint
// with the the apikey
func (c EOSKPIPublisher) getMetric(ctx context.Context, wg *sync.WaitGroup, jobsC <-chan MetricJob, resC chan<- MetricResult, errC chan<- error) {
	ctx, span := trace.StartSpan(ctx, "getMetric")
	defer span.End()
	defer wg.Done()

	// Custom HTTP Transport because of invalid cert.
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Transport: tr,
		Timeout:   time.Duration(c.Timeout) * time.Second,
	}

	for job := range jobsC {
		log.Debug("Handling job...")
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("query", job.Query),
		}, "Handling job")

		// Prepare the query
		req, err := http.NewRequest("GET", c.GrafanaEndpoint, nil)

		q := req.URL.Query()
		q.Add("format", "json")
		q.Add("from", "-15min")
		q.Add("maxDataPoints", "1")
		q.Add("target", job.Query)
		req.URL.RawQuery = q.Encode()

		if err != nil {
			log.Error("Unable to prepare HTTP request:", err.Error())
			errC <- err
			return
		}

		// Add auth header
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.APIKey))

		if debug {
			rd, err := httputil.DumpRequest(req, true)
			if err != nil {
				log.Error("Unable to dump HTTP Request: ", err)
			}
			log.Debug("HTTP Request:\n", string(rd))
		}

		res, err := client.Do(req)
		if err != nil {
			log.Error("Unable to connect to remote host: ", err)
			errC <- err
			return
		}
		defer res.Body.Close()

		if debug {
			rd, err := httputil.DumpResponse(res, true)
			if err != nil {
				log.Error("Unable to dump HTTP Response: ", err)
			}
			log.Debug("HTTP Response:\n", string(rd))
		}

		// Read result
		jsonData, err := ioutil.ReadAll(res.Body)

		if err != nil {
			log.Error("Unable to fetch JSON:", err)
			errC <- err
			return
		}

		// JSON Decoding
		var data []JSONMetric
		err = json.Unmarshal([]byte(jsonData), &data)

		if err != nil {
			log.Error("Unable to unmarshal JSON:", err)
			errC <- err
			return
		}

		// Check if we have data... and throw error if not
		if len(data[0].Datapoints) == 0 {
			errC <- errors.New("No data")
			return
		}

		// We're only interested in the datapoints
		dp := data[0].Datapoints

		// Check that value is != 0
		if dp[len(dp)-1][0] == 0 {
			errC <- errors.New("Null data")
			return
		}

		// Return last good value
		resC <- MetricResult{
			Instance: job.Instance,
			Key:      job.Key,
			Value:    dp[len(dp)-1][0],
		}
	}
}

func unique(strSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range strSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

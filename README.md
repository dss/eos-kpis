# EOS-KPIs

## Purpose

This tool runs as a daemon and fetches at regular intervals configurable metrics from a Graphite-compatible backend
through the Grafana proxy API and feeds them to a [MONIT KPIs-compatible](http://cern.ch/monit-docs/ingestion/service_metrics.html) endpoint

## Configuration

`eos-kpis` uses [viper](https://github.com/spf13/viper) to parse its configuration. It can be written in any format you like (`JSON`, `YAML`, `TOML`, etc...) with an example below.

```yaml
---
timeout: 5 # Timeout for various operations
interval: 300 # When running in "daemon" mode

apikey: CHANGEME # Grafana API key
grafanaEndpoint: https://filer-carbon.cern.ch/grafana/api/datasources/proxy/1/render
KPIendpoint: http://monit-metrics-dev.cern.ch:10012/
collectorEndpoint: http://jaeger-collector.cern.ch:14268

instances:
  - atlas
  - alice
  - cms
  - lhcb
  - public

metrics:
  ropen: sumSeries(eos.$instance.space.*.sum.stat.ropen)
  wopen: sumSeries(eos.$instance.space.*.sum.stat.wopen)
  bytes_read:  scale(sortByMaxima(eos.$instance.io.bytes_read), 0.0033333)
  bytes_written: scale(scale(sortByMaxima(eos.$instance.io.bytes_written), 0.5), 0.0033333)
```

## Running

The tool supports two running modes:

* Daemon (default): wakes up at regular interval to fetch and push KPIs
* One shot: does what it means :)

Both execution modes support the `--debug` flag that will fill your disk rather quickly if you don't pay attention.

### Example Marathon definition

The tool was originally meant to run on a Mesos cluster by a Marathon job scheduler, see below for a example job definition.

```javascript
{
  "id": "/storage/eos-kpis",
  "cmd": null,
  "cpus": 0.5,
  "mem": 128,
  "disk": 0,
  "instances": 1,
  "acceptedResourceRoles": [
    "*"
  ],
  "container": {
    "type": "DOCKER",
    "volumes": [],
    "docker": {
      "image": "gitlab-registry.cern.ch/dss/eos-kpis:stable",
      "network": "HOST",
      "portMappings": [],
      "privileged": false,
      "parameters": [
        {
          "key": "rm",
          "value": "true"
        }
      ],
      "forcePullImage": true
    }
  },
  "env": {
    "EOS_APIKEY": "eyJXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX=",
    "EOS_KPIENDPOINT": "http://monit-metrics-changeme.cern.ch:10012/"
  },
  "healthChecks": [
    {
      "gracePeriodSeconds": 30,
      "intervalSeconds": 60,
      "timeoutSeconds": 5,
      "maxConsecutiveFailures": 3,
      "portIndex": 0,
      "path": "/live",
      "protocol": "HTTP",
      "ignoreHttp1xx": false
    }
  ],
  "portDefinitions": [
    {
      "port": 10000,
      "protocol": "tcp",
      "labels": {}
    }
  ]
}
```

## Debugging

If something goes wrong or the tool doesn't perform as expected, the tool exposes various routes through an HTTP health-check endpoint on a configurable port:

* `/live`: indicates whether the instance is alive and healthy
* `/ready`: indicates whether new requests can ben handled by the instance.
* `/debug/pprof/{cmdline,profile,...}`: exposes profiling data for analysis with `pprof`
* `/debug/tracez`: exposes traces from each component of the running instance with latency samples
* `/debug/rpcz`: not used, supports only gRPC for now (and we have to use HTTP)